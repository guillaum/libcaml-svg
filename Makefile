OCB_FLAGS = -use-ocamlfind -I src
OCB = ocamlbuild $(OCB_FLAGS)

LIB_FILES = svg.cma svg.cmxa svg.a svg.cmi svg.cmx svg_types.cmi svg_types.cmx
INSTALL_FILES = $(LIB_FILES:%=_build/src/%)

VERSION = `cat VERSION`

build:
	$(OCB) $(LIB_FILES)

install: build uninstall
	ocamlfind install -patch-version $(VERSION) svg META $(INSTALL_FILES)

uninstall:
	ocamlfind remove svg

tool:
	ocamlbuild -use-ocamlfind -pkg "svg, ANSITerminal" -I src_tool svg_tool.native

.PHONY: all clean build

clean:
	$(OCB) -clean
	rm -f svg_tool.native