open Svg_utils
open Svg_types

module Svg: sig
  val set_default_font: string -> unit
  val set_text_pad: int -> unit

  type t

  val height: t -> int

  val width: t -> int

  val translate: (int * int) -> t -> t

  val group: t list -> t

  val rect:
    ?stroke: Color.t ->
    ?stroke_width: int ->
    ?stroke_dasharray: string ->
    ?fill: Color.t ->
    x: int ->
    y: int ->
    width: int ->
    height: int ->
    unit -> t

  val line:
    ?stroke: Color.t ->
    ?stroke_width: int ->
    ?stroke_dasharray: string ->
    x1: int ->
    y1: int ->
    x2: int ->
    y2: int ->
    unit -> t

  val path:
    ?arrow: bool ->
    ?fill: Color.t ->
    ?stroke: Color.t ->
    ?stroke_width: int ->
    ?stroke_dasharray: string ->
    d: string ->
    unit -> t

  val text:
    ?text_anchor: string ->
    ?font_family: string ->
    ?font_size: int ->
    ?vertical_align: string ->
    ?pad: int ->
    ?stroke_width: int ->
    ?fill: Color.t ->
    ?bounding_box: Color.t ->
    ?background: Color.t ->
    x: int ->
    y: int ->
    text: string ->
    unit -> t

  val save: string -> int -> int -> t -> unit
end