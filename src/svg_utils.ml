module List_ = struct
  let rec map_opt f = function
    | [] -> []
    | None :: t -> map_opt f t
    | (Some x)::t -> (f x) :: (map_opt f t)
end

module Font = struct
  let tmp_file = Filename.temp_file "libcaml-svg" ".svg"
  let surface = Cairo.SVG.create ~fname:tmp_file ~width:1000. ~height:1000.
  let surf = Cairo.create surface

  let get_text_dimension ?(bold=false) ?(italic=false) fontname size text =
    Cairo.select_font_face surf fontname
      ~slant:(if italic then Cairo.Italic else Cairo.Upright)
      ~weight:(if bold then Cairo.Bold else Cairo.Normal);
    Cairo.set_font_size surf (float_of_int size) ;
      let text_ext = Cairo.text_extents surf text in
      let font_ext = Cairo.font_extents surf in
      (int_of_float text_ext.Cairo.width,
       int_of_float font_ext.Cairo.ascent,
       int_of_float font_ext.Cairo.descent)
end (* module Font *)

module Color = struct
  type t = string

  let black = "#000000"
  let white = "#ffffff"
  let transparent = "transparent"
end (* module Color *)

