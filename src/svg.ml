open Printf

open Svg_utils
open Svg_types

module Global = struct
  let default_font_family = ref "Verdana"
  let default_font_size = ref 24
  let text_pad = ref 5
end

module Transform = struct
  type t = string list

  let to_string t = String.concat " " t

  let to_att = function
  | [] -> None
  | t -> Some ("transform", to_string t)

  let translate (dx,dy) t = (sprintf "translate (%d,%d)" dx dy) :: t
end

module Rect = struct
  type t = {
    x: int;
    y: int;
    width: int;
    height: int;
    stroke: Color.t option;
    stroke_width: int;
    stroke_dasharray: string option;
    fill: Color.t option;
  }

  let build
    ?stroke
    ?(stroke_width=1)
    ?stroke_dasharray
    ?fill
    ~x ~y ~width ~height () =
      { x; y; width; height; stroke; stroke_width; stroke_dasharray; fill }

  let height t = t.height

  let to_buff buff ?(trans=[]) t =
    let att = String.concat " "
      (
        List_.map_opt (fun (n,v) -> sprintf "%s=\"%s\"" n v)
        [ Transform.to_att trans;
          Some ("x", string_of_int t.x);
          Some ("y", string_of_int t.y);
          Some ("width", string_of_int t.width);
          Some ("height", string_of_int t.height);
          (match t.stroke with None -> None | Some d -> Some ("stroke", d));
          Some ("stroke-width", string_of_int t.stroke_width);
          (match t.stroke_dasharray with None -> None | Some d -> Some ("stroke-dasharray", d));
          Some ("fill", (match t.fill with None -> Color.transparent | Some c -> c));
        ]
      ) in
    bprintf buff "<rect %s/>\n" att
end

module Line = struct
  type t = {
    x1: int;
    y1: int;
    x2: int;
    y2: int;
    stroke: Color.t;
    stroke_width: int;
    stroke_dasharray: string option;
  }

  let build
    ?(stroke=Color.black)
    ?(stroke_width=1)
    ?stroke_dasharray
    ~x1 ~y1 ~x2 ~y2 () =
      { x1; y1; x2; y2; stroke; stroke_width; stroke_dasharray; }

  let height t = abs(t.y2 - t.y1)

  let to_buff buff ?(trans=[]) t =
    let att = String.concat " "
      (
        List_.map_opt (fun (n,v) -> sprintf "%s=\"%s\"" n v)
        [ Transform.to_att trans;
          Some ("x1", string_of_int t.x1);
          Some ("y1", string_of_int t.y1);
          Some ("x2", string_of_int t.x2);
          Some ("y2", string_of_int t.y2);
          Some ("stroke", t.stroke);
          Some ("stroke-width", string_of_int t.stroke_width);
          (match t.stroke_dasharray with None -> None | Some d -> Some ("stroke-dasharray", d));
        ]
      ) in
    bprintf buff "<line %s/>\n" att
end

module Path = struct
  type t = {
    d: string;
    arrow: bool;
    fill: Color.t option;
    stroke: Color.t;
    stroke_width: int;
    stroke_dasharray: string option;
  }

  let build
    ?(arrow=false)
    ?fill
    ?(stroke=Color.black)
    ?(stroke_width=1)
    ?stroke_dasharray
    ~d () =
      { d; arrow; fill; stroke; stroke_width; stroke_dasharray; }

  let to_buff buff ?(trans=[]) t =
    let att = String.concat " "
      (
        List_.map_opt (fun (n,v) -> sprintf "%s=\"%s\"" n v)
        [ Transform.to_att trans;
          Some ("d", t.d);
          Some ("stroke", t.stroke);
          Some ("stroke-width", string_of_int t.stroke_width);
          (match t.stroke_dasharray with None -> None | Some d -> Some ("stroke-dasharray", d));
          Some ("fill", (match t.fill with None -> Color.transparent | Some c -> c));
          if t.arrow then Some ("marker-end", "url(#arrowhead)") else None
        ]
      ) in
    bprintf buff "<path %s/>\n" att
end

module Text = struct
  type t = {
    w: int; h:int; d:int; (* text size *)
    text_anchor: string;
    vertical_align: string; (* top, bottom, baseline *)
    font_family: string;
    font_size: int;
    pad: int;
    stroke_width: int;
    fill: Color.t;
    bounding_box: Color.t option;
    background: Color.t option;
    x: int;
    y: int;
    text: string;
    box: Rect.t option
  }

  let south t = t.y+t.d+t.pad
  let north t = t.y-t.h-t.pad
  let east t = match t.text_anchor with
  | "end" -> t.x - t.w - 2*t.pad
  | "middle" -> t.x-(t.w/2)-t.pad
  | _ -> t.x
  let west t = match t.text_anchor with
  | "end" -> t.x
  | "middle" -> t.x+(t.w/2)+t.pad
  | _ -> t.x + t.w + 2*t.pad

  let width t = t.w+2*t.pad
  let height t = t.h+t.d+2*t.pad
  let above t = t.h+t.pad (* distance from baseline to north *)
  let below t = t.d+t.pad (* distance from baseline to south *)

  let build
    ?(text_anchor="middle")
    ?(font_family = !Global.default_font_family)
    ?(font_size = !Global.default_font_size)
    ?(vertical_align = "baseline")
    ?(pad = !Global.text_pad)
    ?(stroke_width=1)
    ?(fill=Color.black)
    ?bounding_box
    ?background
    ~x ~y ~text () =
      let (w,h,d) = Font.get_text_dimension font_family font_size text in
      let baseline_y = match vertical_align with
      | "top" -> y+h+pad
      | "bottom" -> y-d-pad
      | _ -> y in
      let t = (* temp structure with the box *)
      { w; h; d;
        text_anchor; font_family; font_size; vertical_align; pad;
        stroke_width; fill; bounding_box; background;
        x; y=baseline_y; text; box=None;
      } in
      let box = match (t.bounding_box, t.background) with
      | (None, None) -> None
      | (Some border, None) ->
        Some (Rect.build ~x:(east t) ~y:(north t) ~width:(width t) ~height:(height t) ~stroke:border ())
      | (None, Some bg) ->
        Some (Rect.build ~x:(east t) ~y:(north t) ~width:(width t) ~height:(height t) ~fill:bg ())
      | (Some border, Some bg) ->
        Some (Rect.build ~x:(east t) ~y:(north t) ~width:(width t) ~height:(height t) ~stroke:bg ~fill:bg ()) in
      { t with box }


  (* we make alignement on the bounding box rather than on naked text.
     A shift may be needed! *)
  let shift t = match t.text_anchor with
  | "end" -> -t.pad
  | "middle" -> 0
  | _ -> t.pad

  let to_buff buff ?(trans=[]) t =
    begin
      match t.box with
      | Some t -> Rect.to_buff buff ~trans t
      | None -> ()
    end;
    let att = String.concat " "
      (
        List_.map_opt (fun (n,v) -> sprintf "%s=\"%s\"" n v)
        [ Transform.to_att [];
          Some ("text-anchor", t.text_anchor);
          Some ("font-family", t.font_family);
          Some ("font-size", string_of_int t.font_size);
          Some ("x", string_of_int (t.x + shift t));
          Some ("y", string_of_int t.y);
          Some ("stroke-width", string_of_int t.stroke_width);
          Some ("fill", t.fill); ]
      ) in
    bprintf buff "<text %s>%s</text>\n" att t.text
end

(* Comment *)
module Svg = struct
  let set_default_font font = Global.default_font_family := font
  let set_text_pad value = Global.text_pad := value

  type transform = string list

  type t =
    | Rect of transform * Rect.t
    | Line of transform * Line.t
    | Path of transform * Path.t
    | Text of transform * Text.t
    | Group of transform * t list

  let rec to_buff buff = function
    | Rect (trans, rect) -> Rect.to_buff buff ~trans rect
    | Line (trans, rect) -> Line.to_buff buff ~trans rect
    | Path (trans, rect) -> Path.to_buff buff ~trans rect
    | Text (trans, rect) -> Text.to_buff buff ~trans rect
    | Group ([], t_list) ->
        bprintf buff "<g>\n";
        List.iter (to_buff buff) t_list;
        bprintf buff "</g>\n"
    | Group (trans, t_list) ->
        bprintf buff "<g transform=\"%s\">\n" (Transform.to_string trans);
        List.iter (to_buff buff) t_list;
        bprintf buff "</g>\n"

  let height = function
    | Rect ([], r) -> Rect.height r
    | Line ([], l) -> Line.height l
    | Text ([], t) -> Text.height t
    | _ -> failwith "Cannot compute height"

  let width = function
    | Text ([], t) -> Text.width t
    | _ -> failwith "Cannot compute width"



  let translate (dx, dy) = function
    | Rect (t, e) -> Rect (Transform.translate (dx, dy) t, e)
    | Line (t, e) -> Line (Transform.translate (dx, dy) t, e)
    | Path (t, e) -> Path (Transform.translate (dx, dy) t, e)
    | Text (t, e) -> Text (Transform.translate (dx, dy) t, e)
    | Group (t, e) -> Group (Transform.translate (dx, dy) t, e)

  let group g_list = Group ([], g_list)

  let rect ?stroke ?stroke_width ?stroke_dasharray ?fill ~x ~y ~width ~height () =
    Rect ([], Rect.build ?stroke ?stroke_width ?stroke_dasharray ?fill ~x ~y ~width ~height ())

  let line ?stroke ?stroke_width ?stroke_dasharray ~x1 ~y1 ~x2 ~y2 () =
    Line ([], Line.build ?stroke ?stroke_width ?stroke_dasharray ~x1 ~y1 ~x2 ~y2 ())

  let path ?arrow ?fill ?stroke ?stroke_width ?stroke_dasharray ~d () =
    Path ([], Path.build ?arrow ?fill ?stroke ?stroke_width ?stroke_dasharray ~d ())

  let text
    ?text_anchor ?font_family ?font_size ?vertical_align ?pad
    ?stroke_width ?fill ?bounding_box ?background
    ~x ~y ~text () =
    Text
      ([], Text.build
        ?text_anchor ?font_family ?font_size ?vertical_align ?pad
        ?stroke_width ?fill ?bounding_box ?background
        ~x ~y ~text ()
      )

  let draw_grid buff delta width height =
    for i = 0 to width / delta do
      Line.to_buff buff
        (Line.build
          ~stroke_dasharray: "3 5"
          ~stroke: "#aaaaaa"
          ~x1:(i*delta) ~y1:0 ~x2:(i*delta) ~y2:height ()
      )
    done;
    for i = 0 to height / delta do
      Line.to_buff buff
        (Line.build
          ~stroke_dasharray: "3 5"
          ~stroke: "#aaaaaa"
          ~x1:0  ~y1:(i*delta) ~x2:width ~y2:(i*delta) ()
      )
    done

  let start buff ?grid width height =
    bprintf buff
      "<svg width=\"%d\" height=\"%d\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:svg=\"http://www.w3.org/2000/svg\">\n"
      width height;
    bprintf buff "  <defs>\n";
    bprintf buff "    <marker id=\"arrowhead\" markerWidth=\"4\" markerHeight=\"4\" refX=\"0\" refY=\"2\" orient=\"auto\">\n";
    bprintf buff "      <polygon points=\"0 0, 4 2, 0 4\" />\n";
    bprintf buff "    </marker>\n";
    bprintf buff "  </defs>\n";
    match grid with
    | None -> ()
    | Some delta -> draw_grid buff delta width height

  let stop buff =
    bprintf buff "</svg>\n"

  let save filename width height g =
    let buff = Buffer.create 32 in
    start buff width height;
    to_buff buff g;
    stop buff;
    let out_ch = open_out filename in
      fprintf out_ch "%s" (Buffer.contents buff);
      close_out out_ch

end
