module Color = struct
  type t = string

  let black = "#000000"
  let white = "#ffffff"
  let transparent = "transparent"
end (* module Color *)
