open Printf 
open Log

open Svg

let h_pad = 20 (* space between two back + left & right margin *)

module Draw_dep = struct
  let up y height x_gov x_dep shift_gov shift_dep =
    let x_init = x_gov + 4*shift_gov
    and x_final = x_dep + 4*shift_dep in
    let delta_x = x_final - x_init in
    let d =
      if delta_x > 12
      then
        sprintf
          "M %d %d v %d a6,6 0 0,1 6,-6 h %d a6,6 0 0,1 6,6 v %d"
          x_init (y-2) (3-height) (delta_x-12) (height-7)
    else
      if delta_x < -12
      then
        sprintf
          "M %d %d v %d a6,6 0 0,0 -6,-6 h %d a6,6 0 0,0 -6,6 v %d"
          x_init (y-2) (3-height) (delta_x+12) (height-7)
    else failwith "|delta_x| must be > 12" in
    Svg.path ~stroke_width:2 ~arrow:true ~d ()

  (* return the couple (svg, width) *)
  let pack text_size_list =
    let rec loop y = function
    | [] -> []
    | (text, font_size) :: tail ->
        let svg = Svg.text
          ~background:"#fedcba80"
          (* ~bounding_box:Color.black *)
          ~vertical_align: "top"
          ~text_anchor:"middle"
          ~font_size
          ~x:0 ~y ~text () in
        svg :: (loop (y + Svg.height svg) tail) in
    let svg_list = loop 0 text_size_list in
    (
      Svg.group svg_list,
      List.fold_left (fun acc item -> max acc (Svg.width item)) 0 svg_list
    )
end

let main () =
  Svg.set_default_font "Arial";
  Svg.set_text_pad 1;

  let words = [
    [("Aviator", 24); ("Aviator", 14); ("PROPN", 14)];
    [(",", 24); (",", 14); ("MMMMMmmmzmlkfzmlekfmk", 14)];
    [("un", 24); ("un", 14); ("DET", 14); ("Definite=Ind", 14); ("Gender=Masc", 14); ("Number=Sing", 14); ("PronType=Art", 14)];
  ] in

  let pack_width_list = List.map Draw_dep.pack words in

  let (pack_sequence, total_width) =
    let total_width = ref 0 in
    let rec loop x = function
    | [] -> total_width := x; []
    | (pack, w) :: tail ->
      (Svg.translate (x+w/2,0) pack) :: (loop (x+w+h_pad) tail) in
    let seq = loop h_pad pack_width_list in
      (Svg.group seq, !total_width) in

  let full = Svg.group [
    Svg.translate (0,200) pack_sequence;
    Draw_dep.up 100 30 200 300 1 0;
    Svg.text ~font_size: 12 ~x:250 ~y:60 ~text:"nsubj" ();
    Draw_dep.up 100 50 400 200 0 (-1);
    Svg.text ~font_size: 12 ~x:300 ~y:40 ~text:"nsubj" ();
  ] in

  Svg.save "azerty.svg" total_width 480 full;
  Log.info "azerty.svg saved";
  ()

let _ = main ()